package com.example.qr_scanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultScreen extends AppCompatActivity {

    @BindView(R.id.result) TextView result;
    @BindView(R.id.scanAgain) Button scanAgain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_screen);
        Intent intent = getIntent();
        String text = intent.getStringExtra("text");
        ButterKnife.bind(this);
        result.setText(text);

        scanAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultScreen.this, ScanActivity.class);
                startActivity(intent);
            }
        });
    }
}
